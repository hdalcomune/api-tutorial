# JAVA API test tutorial
### JUnit + RestAssured + Hamcrest
Henrique Dalcomune - hdalcomune@avenuecode.com

Mirna Silva - msilva@avenuecode.com

## Table of Contents
[1. Getting Started](#getting-started)  
[2. Requirements](#requirements)  
[3. Getting your Trello Secrets](getting-your-trello-secrets)  
[4. Creating your project](creating-your-project)  
[5. Creating the first test](creating-the-first-test)  
[6. Adding a new Board test](adding-a-new-board-test)  
[7. Moving configuration to a properties file](moving-configuration-to-a-properties-file)  
[8. Adding a new test for the List endpoint](adding-a-new-test-for-the-list-endpoint)  
[9. Your turn to work](your-turn-to-work)   

## Getting Started
This tutorial is an introduction on how to create automated tests for any API. As you should know there are many different 
approaches to build a framework for API testing. The purpose of this one is not to show all best practices,
but to give an overview on how it works and have a first contact with it. Please contact us if you have any doubts. 

## Requirements 
- Install the latest version of Java Development Kit, you can download from Oracle website.
- Also install Maven, on Mac you can also install it using Brew package manager. You can check the Java and Maven versions by typing: `mvn --version`.
- Install IntelliJ Idea Community or use any other IDE/editor that you feel comfortable with. 
- We'll be using Trello to do this tutorial, so you will need to obtain the Trello token from your account, check the next topic.

## Getting your Trello Secrets
To obtain the token make sure you're logged in to a Trello account, then access the [Trello Developer Keys page] (https://trello.com/app-key). Take note of your *developer key*, scroll down to get your OAuth secret and at the top of the page manually generate a token, we'll need all three to authenticate. Remember to don't share the API Token, since it gives access to all your account data. 

## Creating your project
Start IntelliJ and create a new project, select a Maven Project as shown below:

![alt text](images/1-wizard.png)

On the next window set the groupId and artifactId:

![alt text](images/2-wizard.png)

To complete, choose the project name and its location then click on Finish.

As you probably know the project dependencies are stored inside the pom.xml file, located in the root folder of your project. 
This file contains all the information necessary to maven to build your project. See [Maven's POM Reference](https://maven.apache.org/pom.html), if you have doubts. 

Let's add the dependencies do the pom file, let's check each one before moving forward. We'll use:

- [JUnit](http://junit.org/junit4/) - The test framework that will run the tests.
- [Hamcrest](https://code.google.com/archive/p/hamcrest/wikis/Tutorial.wiki) - Hamcrest is a framework for writing matcher objects, giving more flexibility to do assertions.
- [Rest Assured](http://rest-assured.io/) - A DSL that simplifies the REST based tests.
- [ScribeJava](https://github.com/scribejava/scribejava) - A OAuth client in Java.


| GroupID               | ArtifactID      | Version  |
| -------------         |:-------------:  | ------:  |
| junit                 | junit           | 4.12     |
| org.hamcrest          | hamcrest-junit  | 2.0.0.0  |
| io.rest-assured       | rest-assured    | 3.0.1    |
| com.github.scribejava | scribejava-apis | 2.5.3    |


```xml
<dependencies>
  	<dependency>
  		<groupId>junit</groupId>
  		<artifactId>junit</artifactId>
  		<version>4.12</version>
  	</dependency>
  	<dependency>
  		<groupId>org.hamcrest</groupId>
  		<artifactId>hamcrest-junit</artifactId>
  		<version>2.0.0.0</version>
  	</dependency>
  	<dependency>
  		<groupId>io.rest-assured</groupId>
  		<artifactId>rest-assured</artifactId>
  		<version>3.0.1</version>
  	</dependency>
    <dependency>
      <groupId>com.github.scribejava</groupId>
      <artifactId>scribejava-apis</artifactId>
      <version>2.5.3</version>
    </dependency>
    <dependency>
      <groupId>org.json</groupId>
      <artifactId>json</artifactId>
      <version>20170516</version>
    </dependency>
  </dependencies>
```

After adding the dependencies on your pom.xml file, make sure that IntelliJ downloaded and recognized all dependencies. 

If for some reason your dependencies are underlined with red then "Houston, we have a problem!". To fix that, you need to remove your .m2 file and proceed with a clean package installation. You can do that by running the following commands:

 `rm -rf ~/.m2`
 
 `mvn -U clean package`


## Creating the first test

Let's create a first test that retrieves all the boards that we have in Trello. Create a class named *TrelloTest*, let's add a few imports which will be used in our first test. 

```java
import org.junit.Test;

import static io.restassured.RestAssured.given;
``` 

We are importing JUnit which will be needed to declare the test and also RestAssured. Note that, RestAssured is being imported statically, RestAssured documentation recommends it in order to use it more effectively.

To create our test create a method **getAllBoards**, the method will not return a value and it will be public. Also, we're going to in include the `@test` annotation above the method. The code should look like this:

```java
    @Test
    public void getAllBoards() {

    }
```

When we add the `@test` annotation, basically, we're telling JUnit that the *public void* method can be run as a test case. Now, create our test inside the method. We need to test if the following URL, https://api.trello.com/1/members/me/boards, returns status code *200* when we do a Get operation. After we create we can simply run the test by right-clicking the method name (if you're using IntelliJ) or running as a JUnit test. You can see the code below: 

```java
    @Test
    public void getAllBoards() {
      given().when().get("https://api.trello.com/1/members/me/boards").then().statusCode(200);
    }
```
If you check the result you'll get something like this:

```java
java.lang.AssertionError: 1 expectation failed.
Expected status code <200> doesn`t match actual status code <400>.
```

We got an error, that happened because we're not sending a valid token. Let's fix that! Trello authentication uses OAuth, which is a open protocol that allows secure authorization, we will use all the keys that we saved back in the beggining of the tutorial. Let's create three constants containing the keys that we captured. 

```java
    private static String KEY = ""; //Here goes your Developer Key
    private static String OAUTHSECRET = ""; // OAuth secret
    private static String PERSONALTOKEN = ""; // Your personal account token;
```

We'll need to add the `auth()` method to our test using the constants we created: 

```java
given().auth()
      .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
      .when().get("https://api.trello.com/1/members/me/boards").then().statusCode(200);
```

Now if we try to run again we'll get the following output: 

```java
Process finished with exit code 0
```

That means it passed, we can now access any endpoint from Trello and create more tests. 

## Adding a new Board test

One thing that is useful when testing REST applications is testing response codes, that already gives a feedback if the application behaviour is correct. Let's continue and create more tests. 

Now let's take a look the board creation endpoint: 

| Info               | Method | Endpoint        | Parameters      | 
| -------------      | :----: | :-------------: | :-------------: | 
| Create a new board | POST   | /1/boards/      | name            | 

Response: Status should be 200 (OK). The response should contain a parameter *name* containing the chosen name for the task.

Let's create a new test, create a method `createNewBoard` with the test annotation and build the restUtility test. In the end, your code should look like this:

```java
    @Test
    public void createNewBoard() {
        Map<String, String> postBody = new HashMap<String, String>();
        String boardName = "TestBoard";
        given().auth()
            .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
            .given()
            .contentType(ContentType.JSON)
            .post("https://api.trello.com/1/boards/?name="+ boardName)
            .then().statusCode(200);
    }
```
Now if we run our code the test should pass. But there's one more thing to do, this test just checks for the status code, we still need to verify if the response contains a *name* attribute containing the board name we chose. Let's do that by adding another validation at the end of the test. 

Start by importing a new static dependency to the test: `import static org.hamcrest.core.IsEqual.equalTo;`. Now we can assert if the response body contains the attibute name with same value we are passing. 

```java
given().auth()
        .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
        .given()
        .contentType(ContentType.JSON)
        .post("https://api.trello.com/1/boards/?name="+ boardName)
        .then().statusCode(200)
        .and().body("name",equalTo(boardName));
```

Note that we're including a new Rest Assured keyword, `and()`. The `body()` method is used to retrieve the response content and assert with `equalTo`.

Try running the test again it should pass, but also go and change the assertion value to an invalid one. The final code should look like this:

```java
    @Test
    public void createNewBoard() {
        String boardName = "TestBoard";
        given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .given()
                .contentType(ContentType.JSON)
                .post("https://api.trello.com/1/boards/?name="+ boardName)
                .then().statusCode(200)
                .and().body("name",equalTo(boardName));
    }
``` 

## Moving configuration to a properties file

Now with this code ready, there are some points that can be improved. Did you noticed that we're repeating the host across our tests? If we had a greater number of tests and the host changes we would have to change each test. To avoid that we can use a property file. Let's see how it works.

Properties are configuration values managed as key/value pairs, we can create a property for the host and retrieve it from its key. Let's create a property file to store the hostname and reorganize our folder structure. Consider the following folder structure: 

```
.
+-- src
|   +-- test
|       +-- java
|           +-- TrelloTest.java
|       +-- resources
|           +-- config.properties
+-- .gitIgnore
+-- pom.xml
+-- README.md
```

Let's create a new package named resources under `./src/test` and inside it create a new properties file named `config.properties`. For now, the file will have just one property, `host = https://api.trelo.com`. Inside the `TrelloTest.java` create a new String variable `propFileName` which will have the name of the property file created. Also, declare a new String variable named `host` we'll be using it later. Your changes and should look like this: 

```java
public class TrelloTest {

    String propFileName = "config.properties";
    String host = "";
```

The property file is not being loaded yet but we can replace the hostname in all tests to use the *host* variable. The when statements will have the host variable, like in this example: `.when().get(host + "/1/members/me/boards")`.

Before we continue, do you remember you used the `@Test` annotation for the two tests created? That annotation is from JUnit that also contains other annotations. Let's have a look at each one: 

- `@BeforeClass` – Run once before any of the test methods in the class, `public static void`
- `@AfterClass` – Run once after all the tests in the class have been run, `public static void`
- `@Before` – Run before `@Test`, `public void`
- `@After` – Run after `@Test`, `public void`
- `@Test` – This is the test method to run, `public void`

Since we declared the *host* variable in the class scope but empty, it is necessary to retrive the host value from the properties file. We can do it by using the `@Before` annotation that will be executed at the beginning of each test. Start by declaring a new method named *setUp* that will be `public void`. Inside it we'll be loading the file. 

```java
@Before
    public void setUp() {
    }
```

To manage a properties file we'll be creating instances of `java.util.Properties`. This class extends `java.util.Hashtable` and will allows us to: 

- load key/value pairs into a Properties object from a stream,
- retrieve a value from its key,
- and, save the properties to a stream.

Start by declaring a new *Properties* object in the `setUp()` method and initialize it. Also, do the same for a *InputStream*, we'll be using it to read the config file. Then, use the `load()` method sending the *inputStream* as a parameter to retrieve the key/value pairs and get it with the `getProperty()` method. Your code should look like this:

```java 
@Before
    public void setUp() throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        properties.load(inputStream);
        host = properties.getProperty("host");
    }
```

Note that the method has the `IOException` in its signature due to the *InputStream* object. Also, there are some new imports:

```java
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties
```

Try running you test again; the final code should look like this: 

```java
/**
 * Created by ac-hdalcomune on 12/08/17.
 */

import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class TrelloTest {

    String propFileName = "config.properties";
    String host = "";

    private static String KEY = "";
    private static String OAUTHSECRET = "";
    private static String PERSONALTOKEN = "";


    @Before
    public void setUp() throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        properties.load(inputStream);
        host = properties.getProperty("host");
    }

    @Test
    public void getAllBoards() {

        given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when().get(host + "/1/members/me/boards")
                .then().statusCode(200);
    }

    @Test
    public void createNewBoard() {
        String boardName = "TestBoard";
        given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .given()
                .contentType(ContentType.JSON)
                .post(host + "/1/boards/?name="+ boardName)
                .then().statusCode(200)
                .and().body("name",equalTo(boardName));
    }
}
```

## Adding a new test for the List endpoint

In Trello, users can create lists which are inside boards, let's create a test to insert a new list. Let's have a look at the endpoint details:

Endpoint: `/1/lists?name=name&idBoard=idBoard`

| Info               | Method | Endpoint        | Parameters      | 
| -------------      | :----: | :-------------: | :-------------: | 
| Create a new list  | POST   | /1/lists/       | name, idBoard   | 

Note that, differently from the */board* endpoint this one has two mandatory parameters, the *name* of the list and the *idBoard* from the board where the list will be created. For that, we will need to have a Board ID before creating a new list, our new test can follow these steps:

- Create a new board
- Retrieve the boardID
- Create a new list
- Make the proper assertions

Basically, we need to repeat the code that we have in the *createNewBoard* test, but this time it is necessary to capture the Board ID that needs to be passed to the *List* endpoint. We already added `org.json` as a Maven dependency to extract the JSON attribute corresponding the board ID. Let's repeat the code to create the board but assigning it to a variable, `ValidatableResponse response`.

```java
ValidatableResponse response = given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when()
                .contentType(ContentType.JSON)
                .post(host + "/1/boards/?name="+ boardName)
                .then().statusCode(200)
                .and().body("name",equalTo(boardName));
```

Now, get the response body in JSON format and extract the `id` attribute from it: 

```java
JSONObject body = new JSONObject(response.extract().body().asString());
String boardId = body.getString("id");
```

Finally, with the Board ID captured the *List* endpoint can be tested. The test should be created like the others, validate the status code, that should be *200* and the attribute *name* should match the one chosen. The final code should look like this:

```java
@Test
    public void createNewList() {
        String boardName = "BoardWithList";
        String listName = "NewList";

        ValidatableResponse response = given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when()
                .contentType(ContentType.JSON)
                .post(host + "/1/boards/?name="+ boardName)
                .then().statusCode(200)
                .and().body("name",equalTo(boardName));

        JSONObject body = new JSONObject(response.extract().body().asString());
        String boardId = body.getString("id");

        given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when()
                .contentType(ContentType.JSON)
                .post(host + "/1/lists/?name="+listName+"&idBoard="+boardId)
                .then().statusCode(200)
                .and().body("name",equalTo(listName));;
    }
```

## Your turn to work

Considering what you built so far, complete and add more tests following the exercises below. 

### Exercise 1 
The `/boards` endpoint should not be used when sending invalid credentials. Can you identify which of the parameters are being sent that authenticates in Trello app and create a test that asserts that no info comes back? The status *401 - forbidden* should be returned from the API.

### Exercise 2
Create a test for the following story cards below:

#### Story 01 - Create new card

Endpoint: `https://api.trello.com/1/cards?idList={idList}`

| Info               | Method | Endpoint        | Mandatory Parameters    | Optional Parameters | 
| -------------      | :----: | :-------------: | :-------------:         |  :-------------:    |
| Create a new list  | POST   | /1/cards/       | idList                  | name, desc          |

Acceptance Criteria:    

- It is mandatory to send a valid *idList* in the card
- *name* is the card name and *desc* is its description, both should be saved when sent. 

#### Story 02 - Close card

Endpoint: `https://api.trello.com/1/cards/{id}?closed={boolean}`

| Info               | Method | Endpoint        | Mandatory Parameters    | Optional Parameters | 
| -------------      | :----: | :-------------: | :-------------:         |  :-------------:    |
| Create a new list  | POST   | /1/cards/       | id                      | closed              |

Acceptance Criteria: 

- An error should be returned when an invalid *id* is sent
- Closed cards should still be retrievable through a GET call to: `/cards/{id}`
- Cards can be enabled back to previous state

#### Story 03 - Delete Card

Endpoint: `https://api.trello.com/1/cards/{id}`

| Info               | Method   | Endpoint        | Mandatory Parameters    | 
| -------------      | :----:   | :-------------: | :-------------:         |
| Create a new list  | DELETE   | /1/cards/       | id                      |

Acceptance Criteria: 

- Once deleted card should not be retrievable through a GET call to: `/cards/{id}`. *404 status* should be returned 

### Exercise 3

All Trello credentials are stored inside the test class, move all three to the `config.properties` file.

### Exercise 4

If you were automating all this through the UI in Trello using any webdriver, how much effort it would be necessary? What are the advantages and disadvantages of each test approach? Create a README section in your project and share your thoughts.

### Challenge

#### A bit of contract testing 

So far we validated functional aspects of Trello API, but when automating tests for any service validation it might be interesting to create contract tests as well. This is a good practice when testing the integration of different services, when one act as a consumer and another as a provider.

In Trello we didn't test integration between different services, that doesn't prevent us to write the tests. But what needs to be tested? A contract test is a validation of the response from a provider service against a schema, that is basically a document describing the attributes and status codes that are expected. So, instead of validating the data from the response we will be comparing the attributes and the document format against a document that can be a json file, for instance.

There are a couple files available in the schemas directory of this repo, can you copy those and create a test to validate the response against the schema provided? 
There are two schemas available, one for the endpoint to create boards and another for the endpoint to create lists. To do that, you will have to include the following dependency inside the *pom.xml* file:

```xml
<dependency>
    <groupId>io.rest-assured</groupId>
    <artifactId>json-schema-validator</artifactId>
    <version>3.0.1</version>
</dependency>
```

Also, you can use another static import from Rest Assured, `io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema`, which can be called from the `body()` method. 

## REFERENCES
### Contract tests
https://martinfowler.com/articles/consumerDrivenContracts.html
### Hamcrest
https://code.google.com/archive/p/hamcrest/wikis/Tutorial.wiki
### JSON
https://developer.android.com/reference/org/json/JSONObject.html
### JUnit
http://junit.org/junit4/
https://www.mkyong.com/unittest/junit-4-tutorial-1-basic-usage/
https://www.tutorialspoint.com/junit/junit_test_framework.htm
### OAuth Authentication standard
https://github.com/scribejava/scribejava
https://oauth.net/about/introduction/
### Rest-Assured
https://github.com/rest-assured/rest-assured
### Trello API
https://trello.com/app-key


