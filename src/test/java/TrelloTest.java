/**
 * Created by ac-hdalcomune on 12/08/17.
 */

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.core.IsEqual.equalTo;

public class TrelloTest {

    String propFileName = "config.properties";
    String host = "";

    private static String KEY = "";
    private static String OAUTHSECRET = "";
    private static String PERSONALTOKEN = "";

    @Before
    public void setUp() throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        properties.load(inputStream);
        host = properties.getProperty("host");
    }

    @Test
    public void getAllBoards() {

        //"key", "secret", "accesskey", "accesssecret"
        given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when().get(host + "/1/members/me/boards")
                .then().statusCode(200);
    }

    @Test
    public void createNewBoard() throws FileNotFoundException {
        String boardName = "TestBoard";
        ValidatableResponse response =
        given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when()
                .contentType(ContentType.JSON)
                .post(host + "/1/boards/?name="+ boardName)
                .then().statusCode(200)
                .and().body("name",equalTo(boardName));

        response.body(matchesJsonSchema(getFileStream("listSchema.json")));
    }

    @Test
    public void createNewList() throws FileNotFoundException {
        String boardName = "BoardWithList";
        String listName = "NewList";

        ValidatableResponse boardResponse = given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when()
                .contentType(ContentType.JSON)
                .post(host + "/1/boards/?name="+ boardName)
                .then().statusCode(200)
                .and().body("name",equalTo(boardName));

        JSONObject body = new JSONObject(boardResponse.extract().body().asString());
        String boardId = body.getString("id");

        ValidatableResponse listResponse = given().auth()
                .oauth(KEY, OAUTHSECRET, PERSONALTOKEN, "")
                .when()
                .contentType(ContentType.JSON)
                .post(host + "/1/lists/?name="+listName+"&idBoard="+boardId)
                .then().statusCode(200)
                .and().body("name",equalTo(listName));

        listResponse.body(matchesJsonSchema(getFileStream("listSchema.json")));
    }

    private String getFileStream(String fileName) throws FileNotFoundException {
        String content = new Scanner(new File("src/test/resources/schemas/"+fileName)).useDelimiter("\\Z").next();
        return content;
    }
}
